package carvajal.parejo.daniel.repository;

import carvajal.parejo.daniel.model.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterRepository extends JpaRepository<Tweet, Long> {

}
