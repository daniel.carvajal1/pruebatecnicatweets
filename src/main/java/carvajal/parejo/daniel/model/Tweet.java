package carvajal.parejo.daniel.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tweet")
@NoArgsConstructor
public class Tweet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "usuarName", nullable = false)
    private String usuarName;
    @Column(name = "lang", nullable = false)
    private String lang;
    @Column(name = "text", nullable = false)
    private String text;
    @Column(name = "validate", nullable = false)
    private Boolean validate;
    @Column(name = "follower", nullable = false)
    private Integer followers;
    @Column(name = "hashtags ", nullable = false)
    private String hashtags;

    public Tweet(String usuarName, String lang, String text, Boolean validate, int followers, String hashtags) {
        this.usuarName = usuarName;
        this.lang = lang;
        this.text = text;
        this.validate = validate;
        this.followers = followers;
        this.hashtags = hashtags;
    }
}
