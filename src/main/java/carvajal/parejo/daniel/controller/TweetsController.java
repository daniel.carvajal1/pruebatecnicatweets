package carvajal.parejo.daniel.controller;

import carvajal.parejo.daniel.exception.ResourceNotFoundException;
import carvajal.parejo.daniel.model.Tweet;
import carvajal.parejo.daniel.repository.TwitterRepository;
import carvajal.parejo.daniel.service.TwitterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/tweets/")
public class TweetsController {

    @Autowired
    private TwitterRepository twitterRepository;

    @Autowired
    private TwitterService twitterService;

    @GetMapping("/bbdd")
    public List<Tweet> getAllTweetsBBDD() {
        return twitterRepository.findAll();
    }


    @GetMapping("/recovery/{id}")
    public ResponseEntity<Tweet> getEmployeeById(@PathVariable(value = "id") Long tweetId)
            throws ResourceNotFoundException {
        Tweet employee = twitterRepository.findById(tweetId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + tweetId));
        return ResponseEntity.ok().body(employee);
    }

    @GetMapping("/save")
    public List<Tweet> createTweets() {
        return twitterRepository.saveAll(twitterService.selectTweets());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Tweet> updateTweet(@PathVariable(value = "id") Long tweetId,
                                             @Valid @RequestBody Tweet tweetDetails) throws ResourceNotFoundException {
        Tweet tweet = twitterRepository.findById(tweetId)
                .orElseThrow(() -> new ResourceNotFoundException("Tweet not found for this id :: " + tweetId));
        tweet.setValidate(tweetDetails.getValidate());
        final Tweet updatedEmployee = twitterRepository.save(tweet);
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/deleteall")
    public Map<String, Boolean> deleteTweetAll() {
        twitterRepository.deleteAll();
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted all", Boolean.TRUE);
        return response;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String, Boolean> deleteTweet(@PathVariable(value = "id") Long tweetId)
            throws ResourceNotFoundException {
        Tweet tweet = twitterRepository.findById(tweetId)
                .orElseThrow(() -> new ResourceNotFoundException("Tweet not found for this id :: " + tweetId));

        twitterRepository.delete(tweet);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/hashtags")
    public Map<String, Integer> hashtags() {
        return twitterService.hashtagsPopulated();
    }
}