package carvajal.parejo.daniel.service;

import carvajal.parejo.daniel.model.Tweet;

import java.util.List;
import java.util.Map;

public interface TwitterService {
    List<Tweet> selectTweets();

    Map<String, Integer> hashtagsPopulated();
}
