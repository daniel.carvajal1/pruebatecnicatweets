package carvajal.parejo.daniel.service;


import carvajal.parejo.daniel.constant.Constants;
import carvajal.parejo.daniel.model.Tweet;
import carvajal.parejo.daniel.repository.TwitterRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import twitter4j.*;

import java.net.URI;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service(value = "twitterService")
public class TwitterServiceImpl implements TwitterService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${twitter.valid.followers}")
    private Integer minFollowers;

    @Value("${twitter.valid.lang}")
    private String language;

    @Value("${twitter.url}")
    private String ePTwitter;

    @Value("${twitter.authorization.bearer}")
    private String keyBearer;

    @Autowired
    private TwitterRepository twitterRepository;

    @Override
    public List<Tweet> selectTweets() {
        List<Tweet> tweetsExternal = getTweets();
        List<Tweet> tweetsValid = new ArrayList<>();
        String[] languagesValid = language.split(Constants.SPLIT_CUTE);

        tweetsExternal.forEach(
                tweet -> {
                    if (tweet.getFollowers() > minFollowers) {
                        Boolean isValid = Boolean.FALSE;
                        String[] langs = tweet.getLang().split(Constants.SPLIT_CUTE);
                        for (String lang : langs) {
                            if (Arrays.stream(languagesValid).anyMatch(lang::equalsIgnoreCase)) {
                                isValid = Boolean.TRUE;
                                break;
                            }
                        }
                        if (isValid) {
                            tweetsValid.add(tweet);
                        }
                    }
                });
        return tweetsValid;
    }

    @Override
    public Map<String, Integer> hashtagsPopulated() {
        List<Tweet> tweetsExternal = getTweets();
        Map<String, Integer> hashtagsPopulated = new HashMap<>();
        AtomicReference<Boolean> hashtagIsFound = new AtomicReference<>(Boolean.FALSE);

        for (Tweet tweet : tweetsExternal) {
            for (String tag : tweet.getHashtags().split(Constants.SPLIT_CUTE)) {
                hashtagIsFound.set(Boolean.FALSE);
                if (hashtagsPopulated.isEmpty()) {
                    hashtagsPopulated.put(tag, 0);
                }
                Map<String, Integer> finalHashtagsPopulated = hashtagsPopulated;
                hashtagsPopulated.forEach((key, value) -> {
                    if (tag.equals(key)) {
                        finalHashtagsPopulated.put(key, finalHashtagsPopulated.get(key) + 1);
                        hashtagIsFound.set(Boolean.TRUE);
                    }
                });
                if (!hashtagIsFound.get()) {
                    hashtagsPopulated.put(tag, 1);
                }
            }
        }
        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();

        hashtagsPopulated.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));

        hashtagsPopulated = sortedMap;
        return hashtagsPopulated;
    }

    private List<Tweet> getTweets() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + keyBearer);
        HttpEntity request = new HttpEntity(headers);
        //He puesto el unico EP que me dejaba hacer algo
        URI uri = UriComponentsBuilder.fromUriString(ePTwitter)
                .build()
                .encode()
                .toUri();
        try {
            //TODO - Mapear bién los parametros devueltos cuando se tenga autorización y se cambie al EP verdadero
            ResponseEntity<Tweet[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, request, Tweet[].class);
            return Arrays.asList(responseEntity.getBody());
        } catch (Exception ex) {
            //Esto lo hago porque no se el formato del JSON que me llegaría en verdad, así por ahora me voy a inventar la respuesta
            return getTweetsMock();
        }
    }

    //Datos mockeados
    private List<Tweet> getTweetsMock() {
        return new ArrayList<Tweet>() {{
            add(new Tweet("demonio1", "es", "Como va eso...", Boolean.FALSE, 1600, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,MO"));
            add(new Tweet("Angel2", "usa", "Hola paquito...", Boolean.TRUE, 1600, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,ME"));
            add(new Tweet("Sol5", "es", "Bienvenido al circo del...", Boolean.FALSE, 100, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,MI"));
            add(new Tweet("Luna10", "fr", "Me quede sin sola tras la...", Boolean.TRUE, 900, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,MA"));
            add(new Tweet("Estrella4", "it", "XDD, que alegria todo...", Boolean.FALSE, 1800, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,MU"));
            add(new Tweet("Jorge88", "fr", "por tener un frances", Boolean.FALSE, 1501, "NY,TIU,PET,KIO,KAO,LI,OFF,ON,PEN,TEN,MU"));
        }};
    }

    //Esto era un ejemplo de código pero no he podido probarlo ya que me pide mas autorización
    private void getTweetsWhitStream() {
        StatusListener listener = new StatusListener() {
            public void onStatus(Status status) {
                System.out.println(status.getUser().getName() + " : " + status.getText());
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {

            }

            @Override
            public void onStallWarning(StallWarning warning) {

            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(listener);
        twitterStream.sample();
    }
}
